#!/usr/bin/env python3

import os
import requests
import sys
from requests.auth import HTTPBasicAuth
from datetime import datetime
api_url = 'https://jenkins.devops-global.idemia.io/job/mID_space/job/mid_nightly/job/mid-repo/wfapi/runs/'
credentials_path = os.getenv("CREDENTIALS_PATH")

def log_info(*args):
    print(f"[{datetime.now()}] [INFO] {' '.join(list(map(lambda a: str(a), args)))}")

def log_error(*args):
    print(f"[{datetime.now()}] [ERROR] {' '.join(list(map(lambda a: str(a), args)))}")

def log_error_and_fail(*args):
    log_error(args)
    sys.exit(1)

def get_credentials():
    login = None
    password = None
    with open(credentials_path if credentials_path != None else 'credentials.txt', 'r') as f:
        for line in f:
            if line == None or len(line) == 0:
                continue
            parts = line.strip().split('=')
            key = parts[0]
            value = parts[1]
            if key == 'login':
                login = value
            elif key == 'password':
                password = value

    return login, password


def get_data(credentials):
    log_info('Requesting list of build and stages status from', api_url)
    response = requests.get(
        api_url,
        auth=HTTPBasicAuth(credentials[0].strip(), credentials[1].strip()))

    response_list = response.json()[0]

    # Build NUmber
    build_number = response_list["id"]

    # Time Jenkins finished
    finished_time = response_list["endTimeMillis"]

    owasp = "NO_DATA_FROM_SERVER"
    sonar = "NO_DATA_FROM_SERVER"

    list_to_iterate = response_list["stages"]

    for elem in list_to_iterate:
        if elem["name"] == "OWASP":
            owasp = elem["status"]
        if elem["name"] == "SonarQube Issue Analysis":
            sonar = elem["status"]

    print(build_number + " " + str(finished_time) + " " + owasp + " " + sonar)


if __name__ == '__main__':
    credentials = get_credentials()
    get_data(credentials)
